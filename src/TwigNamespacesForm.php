<?php

/**
 * @file
 * Contains Drupal\twig_namespaces\TwigNamespacesForm.
 */
namespace Drupal\twig_namespaces;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class TwigNamespacesForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'twig_namespaces.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'twig_namespaces_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /** @var \Twig_Environment $twig */
    // $twig = \Drupal::service('twig');
    // $loader = $twig->getLoader();
    // $extensions = $twig->getExtensions();
    // $globals = $twig->getGlobals();

    $namespaces = \Drupal::service('twig_namespaces.register');

    // $config = $this->config('twig_namespaces.adminsettings');

    foreach ($namespaces->twigLoaderConfig as $key => $value) {
      $form[$key] = [
        '#type' => 'item',
        '#title' => '@' . $key,
        '#default_value' => $key,
      ];

      $paths = [];

      foreach($value['paths'] as $path) {
        $paths[] = '<li>' . $path . '</li>';
      }


      $form[$key . '_details'] = [
        '#type' => 'details',
        '#open' => false,
        '#title' => 'paths',
        '#children' => [
          '#markup' => '<ul>' . join('', $paths) . '</ul>',
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    // $this->config('twig_namespaces.adminsettings')
    //   ->set('twig_namespaces_message', $form_state->getValue('twig_namespaces_message'))
    //   ->save();
  }

}
